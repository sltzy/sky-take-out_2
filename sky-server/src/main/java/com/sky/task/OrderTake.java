package com.sky.task;

import com.sky.entity.Orders;
import com.sky.mapper.OrdersMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@Slf4j
public class OrderTake {

    @Autowired
    private OrdersMapper ordersMapper;

    /**
     * 每分钟执行一次 ，未支付的订单超过15分钟，变更为取消订单
     */
    @Scheduled(cron = "0 * * * * ?") //每分钟执行一次
//    @Scheduled(cron = "5/10 * * * * ?")
    public void noPay(){
        log.info("超时订单，取消订单：{}", LocalDateTime.now());
        //select * from orders where status=? and order_time < (当前时间-15)
        LocalDateTime time = LocalDateTime.now().plusMinutes(-15);
        List<Orders> ordersList= ordersMapper.getByStatusAndCreateTimeLT(Orders.PENDING_PAYMENT,time);
        if (ordersList != null && ordersList.size() > 0) {
            for (Orders orders : ordersList) {
                orders.setStatus(Orders.CANCELLED);
                orders.setCancelReason("订单超时未支付");
                orders.setOrderTime(LocalDateTime.now());
                ordersMapper.update(orders);
            }
        }
    }

    /**
     * 处理派送中的订单
     */
    @Scheduled(cron = "0 0 1 * * ?") //每分钟执行一次
//    @Scheduled(cron = "5/10 * * * * ?")
    public void noComplete(){
        log.info("派送中的订单 改为已完成：{}", LocalDateTime.now());
        //select * from orders where status=? and order_time < (当前时间-15)
        LocalDateTime localDateTime = LocalDateTime.now().plusMinutes(-60);
        List<Orders> ordersList= ordersMapper.getByStatusAndCreateTimeLT(Orders.DELIVERY_IN_PROGRESS,localDateTime);
        if (ordersList != null && ordersList.size() > 0) {
            for (Orders orders : ordersList) {
                orders.setStatus(Orders.COMPLETED);

                ordersMapper.update(orders);
            }
        }
    }






}
