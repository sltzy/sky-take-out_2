package com.sky.service.impl;

import com.sky.dto.GoodsSalesDTO;
import com.sky.entity.Orders;
import com.sky.mapper.OrdersMapper;
import com.sky.mapper.ReportMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.ReportService;
import com.sky.service.WorkspaceService;
import com.sky.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ReportServiceImpl implements ReportService {

    @Autowired
    private WorkspaceService workspaceService;
    @Autowired
    private ReportMapper reportMapper;
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private UserMapper userMapper;


    /**
     * 营业额统计
     * @param begin
     * @param end
     * @return
     */
    @Override
    public TurnoverReportVO turnoverStatistics(LocalDate begin, LocalDate end) {
        //日期，以逗号分隔，例如：2022-10-01,2022-10-02,2022-10-03
        ArrayList<LocalDate> dateList = new ArrayList<>();
        dateList.add(begin);
        while (!begin.equals(end)) {
            begin=begin.plusDays(1);
            dateList.add(begin);
        }
        //营业额，以逗号分隔，例如：406.0,1520.0,75.0
        ArrayList<Double> turnoverList = new ArrayList<>();
        for (LocalDate today : dateList) {
            LocalDateTime beginTime = LocalDateTime.of(today, LocalTime.MIN);
            LocalDateTime endTime = LocalDateTime.of(today, LocalTime.MAX);
            Integer status = Orders.COMPLETED; //已完成的订单
            //把参数封装成map
            HashMap<String,Object> map = new HashMap<>();
            map.put("status",status);
            map.put("beginTime",beginTime);
            map.put("endTime",endTime);
             Double  turnover = ordersMapper.sumById(map);
             turnover = turnover == null?0.0:turnover;
              turnoverList.add(turnover);
        }

        return TurnoverReportVO.builder()
                .dateList(StringUtils.join(dateList,","))
                .turnoverList(StringUtils.join(turnoverList,","))
                .build();
    }

    /**
     * 用户统计接口
     * @param begin
     * @param end
     * @return
     */

    @Override
    public UserReportVO userStatistics(LocalDate begin, LocalDate end) {
        //日期，以逗号分隔，例如：2022-10-01,2022-10-02,2022-10-03
        ArrayList<LocalDate> dateList = new ArrayList<>();
        dateList.add(begin);
        while (!begin.equals(end)) {
            begin = begin.plusDays(1);
            dateList.add(begin);
        }

        List<Integer> newUserList = new ArrayList<>();
        List<Integer> totalUserList = new ArrayList<>();
            for (LocalDate date : dateList) {
                LocalDateTime beginTime = LocalDateTime.of(date, LocalTime.MIN);
                LocalDateTime endTime = LocalDateTime.of(date, LocalTime.MAX);
                //总用户数量 select count(id) from user where  create_time < ?
                Integer totalUserCount= getUserCount(null, endTime);
                //新增用户数量 select count(id) from user where create_time > ? and create_time < ?
                Integer newUserCount = getUserCount(beginTime, endTime);
                newUserList.add(newUserCount);
                totalUserList.add(totalUserCount);
            }
        return UserReportVO.builder()
                .dateList(StringUtils.join(dateList,","))
                .newUserList(StringUtils.join(newUserList,","))
                .totalUserList(StringUtils.join(totalUserList,","))
                .build();
    }
    private Integer getUserCount(LocalDateTime beginTime, LocalDateTime endTime) {
        HashMap<String,Object> map= new HashMap<>();
        map.put("beginTime", beginTime);
        map.put("endTime", endTime);
        Integer  totalUserCount = userMapper.CountByMap(map);
        return totalUserCount;
    }

    /**
     * 订单统计接口
     * @param begin
     * @param end
     * @return
     */

    @Override
    public OrderReportVO orderStatistics(LocalDate begin, LocalDate end) {
        //日期，以逗号分隔，例如：2022-10-01,2022-10-02,2022-10-03
        ArrayList<LocalDate> dateList = new ArrayList<>();
        dateList.add(begin);
        while (!begin.equals(end)) {
            begin = begin.plusDays(1);
            dateList.add(begin);
        }

        Integer status = Orders.COMPLETED;//已完成的订单
        List<Integer> orderCountlist = new ArrayList<>();
        List<Integer> validorderCountlist = new ArrayList<>();
        for (LocalDate date : dateList) {
            LocalDateTime beginTime = LocalDateTime.of(date, LocalTime.MIN);
            LocalDateTime endTime = LocalDateTime.of(date, LocalTime.MAX);
            // 每日订单总数 select count(*) from orders where create_time>? and create_time<?
            Integer orderCount = getOrderCount(null,beginTime,endTime);
            // 每日有效订单数 select count(*) from orders where create_time>? and create_time<? and status=?
            Integer validOrderCount = getOrderCount(status, beginTime, endTime);

            orderCountlist.add(orderCount);
            validorderCountlist.add(validOrderCount);

        }
        //订单总数
        Integer totalOrderCount = orderCountlist.stream().reduce(Integer::sum).get();
        //有效订单总数
        Integer validOrderCount = validorderCountlist.stream().reduce(Integer::sum).get();
       //订单完成率
        Double orderCompletionRate = 0.0;
        if (totalOrderCount != 0) {
            orderCompletionRate = validOrderCount.doubleValue()/totalOrderCount;
        }

        return OrderReportVO.builder()
                .dateList(StringUtils.join(dateList,","))//日期，以逗号分隔
                .orderCountList(StringUtils.join(orderCountlist,",")) //每日订单数
                .validOrderCountList(StringUtils.join(validorderCountlist,","))//每日有效订单数
                .totalOrderCount(totalOrderCount)//订单总数
                .validOrderCount(validOrderCount)//有效订单总数
                .orderCompletionRate(orderCompletionRate)    //订单完成率
                .build();
    }

    @Override
    public SalesTop10ReportVO top10(LocalDate begin, LocalDate end) {
        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);
        HashMap<String,Object> map = new HashMap<>();
        map.put("beginTime",beginTime);
        map.put("endTime",endTime);
        map.put("status",Orders.COMPLETED);

           List<GoodsSalesDTO> goodsSalesDTO = ordersMapper.top10(map);
           //统计菜品名称
        List<String> nameList = goodsSalesDTO.stream().map(GoodsSalesDTO::getName).collect(Collectors.toList());
        //统计菜品销量
        List<Integer> numberList = goodsSalesDTO.stream().map(GoodsSalesDTO::getNumber).collect(Collectors.toList());


        return SalesTop10ReportVO.builder()
                .nameList(StringUtils.join(nameList,","))
                .numberList(StringUtils.join(numberList,","))
                .build();
    }

    private Integer getOrderCount(Integer status, LocalDateTime beginTime, LocalDateTime endTime) {
        HashMap<String,Object> map=new HashMap<>();
        map.put("beginTime", beginTime);
        map.put("endTime", endTime);
        map.put("status", status);
        Integer orderCount =  ordersMapper.orderCountByMap(map);
        return orderCount;
    }

    /**
     * 导出运营数据报表
     * @param response
     */

    @Override
    public void exportExcel(HttpServletResponse response) {
        LocalDate begin = LocalDate.now().minusDays(30);
        LocalDate end = LocalDate.now().minusDays(1);
     //获取最近30天的运营数据
        BusinessDataVO businessDataVO = workspaceService
                .getBusinessData(LocalDateTime.of(begin,LocalTime.MIN),LocalDateTime.of(end,LocalTime.MAX));
        //加载模板
        InputStream is = ReportServiceImpl.class.getClassLoader().getResourceAsStream("template/运营数据报表模板.xlsx");
        //获取输出流对像
        try {
            XSSFWorkbook excel = new XSSFWorkbook(is);
            //获取sheet对象
            XSSFSheet sheet = excel.getSheetAt(0);
            //获取第二行
            XSSFRow row = sheet.getRow(1);
            //获取第二行第二个单元格
            row.getCell(1).setCellValue(begin + "至" + end);
            //获取第四行
            row = sheet.getRow(3);
            row.getCell(2).setCellValue(businessDataVO.getTurnover());
            row.getCell(4).setCellValue(businessDataVO.getOrderCompletionRate());
            row.getCell(6).setCellValue(businessDataVO.getNewUsers());
            //获取第五行
            row= sheet.getRow(4);
            row.getCell(2).setCellValue(businessDataVO.getValidOrderCount());
            row.getCell(2).setCellValue(businessDataVO.getUnitPrice());
            //准备明细数据
            for (int i = 0; i < 30; i++) {
                LocalDate date = begin.plusDays(i);
                businessDataVO = workspaceService
                        .getBusinessData(LocalDateTime.of(date,LocalTime.MIN),LocalDateTime.of(date,LocalTime.MAX));
                row = sheet.getRow(7+i);
                row.getCell(1).setCellValue(date.toString());
                row.getCell(2).setCellValue(businessDataVO.getTurnover());
                row.getCell(3).setCellValue(businessDataVO.getValidOrderCount());
                row.getCell(4).setCellValue(businessDataVO.getOrderCompletionRate());
                row.getCell(5).setCellValue(businessDataVO.getUnitPrice());
                row.getCell(6).setCellValue(businessDataVO.getNewUsers());
            }
            ServletOutputStream os = response.getOutputStream();
            excel.write(os);
            os.close();
            excel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
